﻿using System;
using PowerPlant.Solutions.Dashboard.Business.Services.Interfaces;
using PowerPlant.Solutions.Dashboard.Cli.Interfaces;
using PowerPlant.Solutions.Dashboard.Cli.IoHelpers.Interfaces;

namespace PowerPlant.Solutions.Dashboard.Cli
{
    internal class ProgramLoop : IProgramLoop
    {
        private bool _exit = false;
        private bool _logOut = false;

        private readonly IInputHelper _inputHelper;
        private readonly IOutputHelper _outputHelper;
        private readonly IUsersService _usersService;

        public ProgramLoop(IInputHelper inputHelper, IOutputHelper outputHelper, IUsersService usersService)
        {
            _inputHelper = inputHelper;
            _outputHelper = outputHelper;
            _usersService = usersService;
        }

        public void Run()
        {
            _usersService.AddDefaultUserIfNeeded();

            while (!_exit)
            {
                LogIn();
                DispatchCommands();
            }
        }

        private void DispatchCommands()
        {
            while (!_logOut && !_exit)
            {
                //dispatch commands
                Console.WriteLine("Logged successfully");
                Console.ReadLine();
                return;
            }
        }

        private void LogIn()
        {
            while(true)
            {
                var name = _inputHelper.GetString("Login: ");
                var password = _inputHelper.GetPassword("Password: ");

                if (_usersService.Authenticate(name, password))
                {
                    break;
                }

                _outputHelper.PrintString("Incorrect user or password. Try again...");
            }
        }
    }
}