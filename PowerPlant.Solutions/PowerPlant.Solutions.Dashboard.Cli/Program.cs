﻿using Ninject;
using PowerPlant.Solutions.Dashboard.Cli.Configs;
using PowerPlant.Solutions.Dashboard.Cli.Interfaces;

namespace PowerPlant.Solutions.Dashboard.Cli
{
    internal class Program
    {
        private static void Main()
        {
            new DiConfig().Kernel.Get<IProgramLoop>().Run();
        }
    }
}