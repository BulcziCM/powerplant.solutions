﻿using System;
using PowerPlant.Solutions.Dashboard.Cli.IoHelpers.Interfaces;

namespace PowerPlant.Solutions.Dashboard.Cli.IoHelpers
{
    internal class ConsoleOutputHelper : IOutputHelper
    {
        public void PrintString(string message)
        {
            Console.WriteLine(message);
        }
    }
}