﻿using System;
using PowerPlant.Solutions.Dashboard.Cli.IoHelpers.Interfaces;

namespace PowerPlant.Solutions.Dashboard.Cli.IoHelpers
{
    internal class ConsoleInputHelper : IInputHelper
    {
        public string GetString(string message)
        {
            Console.Write(message);
            return Console.ReadLine();
        }

        public string GetPassword(string message)
        {
            Console.Write(message);

            var password = string.Empty;
            while (true)
            {
                var key = Console.ReadKey(true);
                if (key.Key == ConsoleKey.Enter)
                {
                    break;
                }
                password += key.KeyChar;
            }

            Console.WriteLine();
            return password;
        }
    }
}