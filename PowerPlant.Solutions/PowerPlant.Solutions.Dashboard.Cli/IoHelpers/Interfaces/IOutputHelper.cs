﻿namespace PowerPlant.Solutions.Dashboard.Cli.IoHelpers.Interfaces
{
    internal interface IOutputHelper
    {
        void PrintString(string message);
    }
}