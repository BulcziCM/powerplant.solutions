﻿namespace PowerPlant.Solutions.Dashboard.Cli.IoHelpers.Interfaces
{
    internal interface IInputHelper
    {
        string GetPassword(string message);
        string GetString(string message);
    }
}