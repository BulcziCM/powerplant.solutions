﻿namespace PowerPlant.Solutions.Dashboard.Cli.Interfaces
{
    internal interface IProgramLoop
    {
        void Run();
    }
}