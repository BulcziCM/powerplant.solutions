﻿using Ninject;
using PowerPlant.Solutions.Dashboard.Business.Services;
using PowerPlant.Solutions.Dashboard.Business.Services.Interfaces;
using PowerPlant.Solutions.Dashboard.Cli.Interfaces;
using PowerPlant.Solutions.Dashboard.Cli.IoHelpers;
using PowerPlant.Solutions.Dashboard.Cli.IoHelpers.Interfaces;
using PowerPlant.Solutions.Dashboard.Data;
using PowerPlant.Solutions.Dashboard.Data.Models;
using PowerPlant.Solutions.Dashboard.Data.Repositories;
using PowerPlant.Solutions.Dashboard.Data.Repositories.Interfaces;

namespace PowerPlant.Solutions.Dashboard.Cli.Configs
{
    internal class DiConfig
    {
        public IKernel Kernel { get; }

        public DiConfig()
        {
            Kernel = new StandardKernel();

            RegisterCliClasses();
            RgisterServices();
            RegisterRepositories();
        }

        private void RegisterCliClasses()
        {
            Kernel.Bind<IProgramLoop>().To<ProgramLoop>();
            Kernel.Bind<IInputHelper>().To<ConsoleInputHelper>();
            Kernel.Bind<IOutputHelper>().To<ConsoleOutputHelper>();
        }

        private void RgisterServices()
        {
            Kernel.Bind<IUsersService>().To<UsersService>();
        }

        private void RegisterRepositories()
        {
            Kernel.Bind<IDataSourceCommands>().To<DashboardDbCommands>();

            Kernel.Bind<IGenericRepository<DashboardUser>>().To<GenericRepository<DashboardUser>>();
        }
    }
}