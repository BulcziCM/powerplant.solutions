﻿namespace PowerPlant.Solutions.Dashboard.Business.Services.Interfaces
{
    public interface IUsersService
    {
        void AddDefaultUserIfNeeded();
        bool Authenticate(string name, string password);
    }
}