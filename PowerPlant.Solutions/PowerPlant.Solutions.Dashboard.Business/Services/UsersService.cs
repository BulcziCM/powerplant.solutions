﻿using System.Linq;
using PowerPlant.Solutions.Dashboard.Business.Services.Interfaces;
using PowerPlant.Solutions.Dashboard.Data.Models;
using PowerPlant.Solutions.Dashboard.Data.Repositories.Interfaces;

namespace PowerPlant.Solutions.Dashboard.Business.Services
{
    public class UsersService : IUsersService
    {
        private DashboardUser LoggedUser;
        private readonly IDataSourceCommands _dataSourceCommands;
        private readonly IGenericRepository<DashboardUser> _usersRepository;

        public UsersService(IDataSourceCommands dataSourceCommands, IGenericRepository<DashboardUser> usersRepository)
        {
            _dataSourceCommands = dataSourceCommands;
            _usersRepository = usersRepository;
        }

        public void AddDefaultUserIfNeeded()
        {
            if (_dataSourceCommands.Exists())
            {
                return;
            }

            _dataSourceCommands.Create();
            _usersRepository.Add(new DashboardUser { Name = "Admin", Password = "Admin", IsAdmin = true });
        }

        public bool Authenticate(string name, string password)
        {
            var user = _usersRepository.GetAll()
                .SingleOrDefault(x => x.Name == name && x.Password == password);

            LoggedUser = user;
            return user != null;
        }
    }
}