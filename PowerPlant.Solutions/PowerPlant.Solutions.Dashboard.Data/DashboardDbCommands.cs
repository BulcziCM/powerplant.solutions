﻿using PowerPlant.Solutions.Dashboard.Data.Repositories.Interfaces;

namespace PowerPlant.Solutions.Dashboard.Data
{
    public class DashboardDbCommands : IDataSourceCommands
    {
        private readonly DashboardDbContext _dbContext;

        public DashboardDbCommands()
        {
            _dbContext = new DashboardDbContext();
        }

        public bool Exists()
        {
            return _dbContext.Database.Exists();
        }

        public void Create()
        {
            if (!Exists())
            {
                _dbContext.Database.Create();
            }
        }
    }
}