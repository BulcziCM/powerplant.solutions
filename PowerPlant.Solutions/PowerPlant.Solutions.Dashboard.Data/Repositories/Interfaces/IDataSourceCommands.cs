﻿namespace PowerPlant.Solutions.Dashboard.Data.Repositories.Interfaces
{
    public interface IDataSourceCommands
    {
        void Create();
        bool Exists();
    }
}