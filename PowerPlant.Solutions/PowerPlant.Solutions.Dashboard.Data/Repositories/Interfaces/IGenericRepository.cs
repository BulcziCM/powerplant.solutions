﻿using System.Linq;
using PowerPlant.Solutions.Dashboard.Data.Models.Interfaces;

namespace PowerPlant.Solutions.Dashboard.Data.Repositories.Interfaces
{
    public interface IGenericRepository<T> where T : class, IEntity
    {
        IQueryable<T> GetAll();
        T Get(int id);
        T Add(T entity);
        void Remove(int id);
        void Remove(T entity);
    }
}