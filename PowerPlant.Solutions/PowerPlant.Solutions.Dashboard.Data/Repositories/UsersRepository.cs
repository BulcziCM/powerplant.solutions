﻿using System.Data.Entity;
using System.Linq;
using PowerPlant.Solutions.Dashboard.Data.Models.Interfaces;
using PowerPlant.Solutions.Dashboard.Data.Repositories.Interfaces;

namespace PowerPlant.Solutions.Dashboard.Data.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class, IEntity
    {
        private readonly DashboardDbContext _dbContext;
        private DbSet<T> DataSet => _dbContext.Set<T>();

        public GenericRepository()
        {
            _dbContext = new DashboardDbContext();
        }

        public IQueryable<T> GetAll()
        {
            return DataSet;
        }

        public T Get(int id)
        {
            return DataSet.SingleOrDefault(x => x.Id == id);
        }

        public T Add(T entity)
        {
            var newUser = DataSet.Add(entity);
            _dbContext.SaveChanges();
            return newUser;
        }

        public void Remove(int id)
        {
            DataSet.Remove(Get(id));
            _dbContext.SaveChanges();
        }

        public void Remove(T entity)
        {
            DataSet.Remove(entity);
            _dbContext.SaveChanges();
        }
    }
}