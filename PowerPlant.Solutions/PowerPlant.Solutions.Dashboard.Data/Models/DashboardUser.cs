﻿using PowerPlant.Solutions.Dashboard.Data.Models.Interfaces;

namespace PowerPlant.Solutions.Dashboard.Data.Models
{
    public class DashboardUser : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
    }
}