﻿namespace PowerPlant.Solutions.Dashboard.Data.Models.Interfaces
{
    public interface IEntity
    {
        int Id { get; }
    }
}