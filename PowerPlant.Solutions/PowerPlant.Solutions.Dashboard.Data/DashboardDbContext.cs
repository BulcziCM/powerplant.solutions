﻿using System.Configuration;
using System.Data.Entity;
using PowerPlant.Solutions.Dashboard.Data.Models;

namespace PowerPlant.Solutions.Dashboard.Data
{
    internal class DashboardDbContext : DbContext
    {
        public DashboardDbContext() : base(GetConnectionString())
        { }

        public DbSet<DashboardUser> UserDbSet { get; set; }

        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["DashboardDatabase"].ConnectionString;
        }
    }
}